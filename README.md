# Project to Generate Binary Tree using XML File


Project Name : Tree-Generator

This maven project to generate a binary tree from an XML

1. Initial Set up

Starting of this project [ Prototype Design ]
Please refer org.flowcon.sample


2. Generic Model

Please use org.flowcon.generic

Step to Execute the project 
  
  1. Run the class 'BinaryTreeGenerator.java'
  
  2. Enter file Name. 
  		Three sample files are in the folder 'employee.xml , books2.xml, books.xml'
  3. Console will print the Binary Tree InOrder  Traversal format
  
 
