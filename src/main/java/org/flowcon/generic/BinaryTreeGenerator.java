package org.flowcon.generic;

import java.awt.List;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.flowcon.gui.PrintTree;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class BinaryTreeGenerator {

	public static void main(String[] args) throws IOException {
		
//
		Scanner scanner  = new Scanner(System.in);	
//		// 1. Read File name input from console
		System.out.println("Enter the File Name [path]: ");
		String fileName  =  scanner.nextLine();

		String tagName = "";
		
		//2. Check File Exists in the location 
		File file =  new File(fileName);
		if(!file.exists()) {
			System.out.println("File Not Exists!! Please enter correct filname/filepath");
			return;
		}
		
		
		//4. Parse XML File in to Document Object
		Document doc  = parseXML(fileName);


		//5. Create the NodeList -> Specify the Node to Create a NodeList 
		
		NodeList nodeList  = getXMLNodeList(doc);
		if(nodeList.getLength() == 0) {
			System.out.println("The Element "+  tagName + " not Exists in the File");
			return;
		}
		
		String root = doc.getDocumentElement().getNodeName();  

	
		
		//6. Traverse through the NodeList and Assign Values to a Generic Object
		BinaryTree binaryTree = addGenericObjectToBinaryTree(nodeList, root);
		
		
		//7. Print the inorder Traversal
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"); 
		binaryTree.inorderTraversal(binaryTree.root); 
		
		PrintTree printTree = new PrintTree();
		printTree.setTree(binaryTree.root);
		PrintStream os = new PrintStream(new File("xml-tree.txt"));
		printTree.print(os);
		 
	}
	
	

	public static Document parseXML(String fileName) {
		Document doc = null;
		try {
			File fXmlFile  = new File(fileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return doc;
		
	}
	
	
	///
	public static NodeList getXMLNodeList(Document doc) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());  
		String tagName ="";
		 NodeList nl = doc.getElementsByTagName("*");
		 if(nl.getLength()>0) {
	    		tagName	= nl.item(1).getNodeName();
			      System.out.println("tag name is : "+tagName);

		 }
		
		NodeList nList = doc.getElementsByTagName(tagName);
		return nList;
	}
	
	private static BinaryTree addGenericObjectToBinaryTree(NodeList nList, String root) throws IOException {
		System.out.println("Number of nodes .................. "+ nList.getLength());
		BinaryTree binaryTree = new BinaryTree();
		ArrayList<Map<String, Object>> nodeMaps = new ArrayList<Map<String, Object>>();
    	Map<String, Object> nodeMap = new HashMap<String, Object>();

	    for (int i=0; i<nList.getLength(); i++) 
	    {
	        Element element = (Element)nList.item(i);
	        
	        String parentTagName = element.getNodeName();
	        
        	nodeMap = new HashMap();

	        for(int j = 0; j< element.getChildNodes().getLength();j++) {

	        	if(j%2 != 0) {
	        		String childTagName = element.getChildNodes().item(j).getNodeName();
	        		int countByTagname = element.getElementsByTagName(childTagName).getLength();
	        		if(countByTagname>1) {
	        			ArrayList<String> childValues =  new ArrayList<String>();
	        			for(int k = 0; k<countByTagname; k++) {

		        			String childValue = element.getElementsByTagName(childTagName).item(k).getTextContent();
			            	childValues.add(childValue);
		        			nodeMap.put(childTagName, childValues);
	        			}
	        		}
	        		else {
	        			String childValue = element.getElementsByTagName(childTagName).item(0).getTextContent();
	        			nodeMap.put(childTagName, childValue);
	        		}
		        }

	        }
	        binaryTree.insert(nodeMap);
	        nodeMaps.add(nodeMap);
	    }
	   
	    createSVGImage(nodeMaps, root);		
	    return binaryTree;
	}
	
	
	public static void createSVGImage(ArrayList<Map<String, Object>> nodeMaps, String root) throws IOException {
		
		StringBuilder svgString = new StringBuilder();

	    svgString.append("<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" height=\"1000\" width=\"1500\">");
		
//	    svgString.append("<ellipse cx=\"200\" cy=\"80\" rx=\"100\" ry=\"50\" style=\"fill:lightblue;stroke-width:0\" />");
//		svgString.append("<text fill=\"black\" font-size=\"20\" font-family=\"Verdana\" x=\"150\" y=\"98\">"+root+"</text>");
//		svgString.append("<line x1=\"200\" y1=\"80\" x2=\"400\" y2=\"80\" style=\"stroke:lightblue;stroke-width:2\" />");

		int cy = 80;
		int y = 100;
		int lineY1 = 80;
		int childLineY = 235;

		for(Map<String, Object> xmlNode: nodeMaps) {
			cy = cy+150;
			y = y+150;
			svgString.append("<line x1=\"400\" y1=\""+childLineY+"\" x2=\"600\" y2=\""+childLineY+"\" style=\"stroke:lightblue;stroke-width:2\" />");
			svgString.append("<line x1=\"400\" y1=\""+lineY1+"\" x2=\"400\" y2=\"200\" style=\"stroke:lightblue;stroke-width:2\" />");
		    svgString.append("<ellipse cx=\"600\" cy=\""+cy+"\" rx=\"100\" ry=\"50\" style=\"fill:lightblue;stroke-width:0\" />");
			svgString.append("<text fill=\"black\" font-size=\"20\" font-family=\"Verdana\" x=\"550\" y=\""+y+"\">"+xmlNode+"</text>");

			childLineY = childLineY + 150;
			lineY1 = lineY1+189;
		}
		
		svgString.append("</svg>");
		
		File svgfile = new File("xmldata.svg");
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(svgfile))) {
		    writer.write(svgString.toString());
		}

	}
	

}

