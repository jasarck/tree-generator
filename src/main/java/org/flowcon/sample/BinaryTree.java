package org.flowcon.sample;

public class BinaryTree {

	 public static class Node{  
	        Book data;  
	        Node left;  
	        Node right;  
	  
	        public Node(Book data){  
	            //Assign data to the new node, set left and right children to null  
	            this.data = data;  
	            this.left = null;  
	            this.right = null;  
	        }  
	      }  
	  
	      //Represent the root of binary tree  
	      public Node root;  
	  
	      public BinaryTree(){  
	          root = null;  
	      }  
	  
	      //insert() will add new node to the binary search tree  
	      public void insert(Book data) {  
	          //Create a new node  
	          Node newNode = new Node(data);  
	  
	          //Check whether tree is empty  
	          if(root == null){  
	              root = newNode;  
	              return;  
	            }  
	          else {  
	              //current node point to root of the tree  
	              Node current = root, parent = null;  
	  
	              while(true) {  
	                  //parent keep track of the parent node of current node.  
	                  parent = current;  
	                  int count = 0;
	                  //If data is less than current's data, node will be inserted to the left of tree  
	                  if(count % 2 == 0) {  
	                      current = current.left;  
	                      if(current == null) {  
	                          parent.left = newNode;  
	                          return;  
	                      }  
	                  }  
	                  //If data is greater than current's data, node will be inserted to the right of tree  
	                  else {  
	                      current = current.right;  
	                      if(current == null) {  
	                          parent.right = newNode;  
	                          return;  
	                      }  
	                  }  
	                  count ++;
	              }  
	          }  
	      }  
	  
	      //inorder() will perform inorder traversal on binary search tree  
	      public void inorderTraversal(Node node) {  
	  
	          //Check whether tree is empty  
	          if(root == null){  
	              System.out.println("Tree is empty");  
	              return;  
	           }  
	          else {  
	  
	              if(node.left!= null)  
	                  inorderTraversal(node.left);  
	              System.out.println(node.data + " ");  
	              if(node.right!= null)  
	                  inorderTraversal(node.right);  
	  
	          }  
	      } 

}
