package org.flowcon.sample;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class BinaryTreeGenerator {

	public static void main(String[] args) {
		
		BinaryTree binaryTree = parseXML();
		binaryTree.inorderTraversal(binaryTree.root); 
		
	}

	public static BinaryTree parseXML(){
		
			BinaryTree binaryTree = new BinaryTree();
		
		try {
			//Make it as any XML
			File fXmlFile  = new File("books2.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());        
			 
			// Specify the Node Name
			NodeList nList = doc.getElementsByTagName("book");
			System.out.println("Size of Node List---------------------------- "+ nList.getLength());

			    for (int temp = 0; temp < nList.getLength(); temp++) {

			        Node nNode = nList.item(temp);
			        Book book = new Book();        

			                
			        if (nNode.getNodeType() == Node.ELEMENT_NODE) {

			            Element eElement = (Element) nNode;
			            
			            book.setIsbn(eElement.getElementsByTagName("isbn").item(0).getTextContent());
			            book.setTitle(eElement.getElementsByTagName("title").item(0).getTextContent());
			            List<String> authors = new ArrayList<String>();
			            int noAuthors = eElement.getElementsByTagName("author").getLength();
			            for(int i = 0; i < noAuthors; i++) {
			            	authors.add(eElement.getElementsByTagName("author").item(i).getTextContent());
			            }
			            book.setAuthor(authors);
			            binaryTree.insert(book);
			        }
			    }
			   
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		 return binaryTree;
	}

	
		
	
}

