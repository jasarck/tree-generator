package org.flowcon.sample;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ListGenerator {

	public static void main(String[] args) {
		
		/*
		 * This class will generate the list of the Book XML
		 * 
		 */
		
		List<Book> books = parseXML();
		for(int i =0; i < books.size(); i++) {
			System.out.println(books.get(i));
		}
		
	}

	private static List<Book> parseXML(){
		
			List<Book> books = null;
		
		try {
			File fXmlFile  = new File("books2.xml");
			System.out.println(fXmlFile.exists());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			
			//Tree code 
			
			
			 doc.getDocumentElement().normalize();
			 
			 books = new ArrayList<Book>();
			
			 System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			            
			 NodeList nList = doc.getElementsByTagName("book");
			            
			 System.out.println("----------------------------");

			    for (int temp = 0; temp < nList.getLength(); temp++) {

			        Node nNode = nList.item(temp);
			        Book book = new Book();        
			        System.out.println("\nCurrent Element :" + nNode.getNodeName());
			                
			        if (nNode.getNodeType() == Node.ELEMENT_NODE) {

			            Element eElement = (Element) nNode;
			            
			            book.setIsbn(eElement.getElementsByTagName("isbn").item(0).getTextContent());
			            book.setTitle(eElement.getElementsByTagName("title").item(0).getTextContent());
			            List<String> authors = new ArrayList<String>();
			            int noAuthors = eElement.getElementsByTagName("author").getLength();
			            for(int i = 0; i < noAuthors; i++) {
			            	authors.add(eElement.getElementsByTagName("author").item(i).getTextContent());
			            }
			            book.setAuthor(authors);
			            books.add(book);
			        }
			    }
			   
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 return books;
	}

	
		
	
}

