package org.flowcon.gui;

import java.io.PrintStream;

import org.flowcon.generic.BinaryTree.Node;

public class PrintTree {
	
	Node tree; 
	
	public void setTree(Node tree) {
		this.tree = tree;
	}

	public String traversePreOrder(Node root) {

	    if (root == null) {
	        return "";
	    }

	    StringBuilder sb = new StringBuilder();
	    sb.append(root.getData());

	    String pointerRight = "└──";
	    String pointerLeft = (root.getRight() != null) ? "├──" : "└──";

	    traverseNodes(sb, "", pointerLeft, root.getLeft(), root.getRight() != null);
	    traverseNodes(sb, "", pointerRight, root.getRight(), false);

	    return sb.toString();
	}
	
	public void traversePreOrder(StringBuilder sb, String padding, String pointer, Node node) {
	    if (node != null) {
	        sb.append(padding);
	        sb.append(pointer);
	        sb.append(node.getData());
	        sb.append("\n");

	        StringBuilder paddingBuilder = new StringBuilder(padding);
	        paddingBuilder.append("│  ");

	        String paddingForBoth = paddingBuilder.toString();
	        String pointerForRight = "└──";
	        String pointerForLeft = (node.getRight() != null) ? "├──" : "└──";

	        traversePreOrder(sb, paddingForBoth, pointerForLeft, node.getLeft());
	        traversePreOrder(sb, paddingForBoth, pointerForRight, node.getRight());
	    }
	}
	
	public void traverseNodes(StringBuilder sb, String padding, String pointer, Node node, 
			  boolean hasRightSibling) {
			    if (node != null) {
			        sb.append("\n");
			        sb.append(padding);
			        sb.append(pointer);
			        sb.append(node.getData());

			        StringBuilder paddingBuilder = new StringBuilder(padding);
			        if (hasRightSibling) {
			            paddingBuilder.append("│  ");
			        } else {
			            paddingBuilder.append("   ");
			        }

			        String paddingForBoth = paddingBuilder.toString();
			        String pointerRight = "└──";
			        String pointerLeft = (node.getRight() != null) ? "├──" : "└──";

			        traverseNodes(sb, paddingForBoth, pointerLeft, node.getLeft(), node.getRight() != null);
			        traverseNodes(sb, paddingForBoth, pointerRight, node.getRight(), false);
			    }
			}	      
	
			public void traversePreOrder(StringBuilder sb, Node node) {
	    	    if (node != null) {
	    	        sb.append(node.getData());
	    	        sb.append("\n");
	    	        traversePreOrder(sb, node.getLeft());
	    	        traversePreOrder(sb, node.getRight());
	    	    }
	    	}
			
			public void print(PrintStream os) {
			    StringBuilder sb = new StringBuilder();
			    traversePreOrder(sb, "", "", this.tree);
			    os.print(sb.toString());
			}
	
	
			public void print(PrintStream os, Node tree) {
					os.print(traversePreOrder(tree));
			}

}
